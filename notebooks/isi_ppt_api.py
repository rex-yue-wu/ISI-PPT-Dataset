# -*- coding: utf-8 -*-
"""
Created on Mon Jul 17 17:42:14 2017

@author: yue_wu

This script defines axillary functions for ISI-PPT dataset
"""
import numpy as np
import os
import unicodedata
import json
from matplotlib import pyplot
# load image I/O lib
use_cv2 = False
use_PIL = False
try :
    import cv2
    use_cv2 = True
except Exception, e :
    import PIL
    use_PIL = True

assert ( use_cv2 or use_PIL  ), "ERROR: cannot load any known common Py2 Image Libs to read and write images\nSupported Libs: cv2, PIL"


def imread( image_file_path ) :
    '''Unified image read function
    INPUT:
        image_file_path = string, path to image file
    OUTPUT:
        image_array = np.ndarray, shape of ( height, width, 3 ), dtype of uint8
    '''
    assert os.path.isfile( image_file_path ), "ERROR: cannot locate image file %s" % image_file_path
    if ( use_cv2 ) :
        image_array = cv2.imread( image_file_path, 1 )
    elif ( use_PIL ) :
        image_array = np.array( PIL.Image.open( image_file_path ) )
    if ( image_array.ndim != 3 ) :
        image_array = np.dstack( [ image_array for k in range(3) ] ) # convert a grayscale image to RGB
    return image_array

def imwrite( image_array, output_file_path ) :
    '''Unified image write function
    INPUT:
        image_array = np.ndarray, shape of ( height, width, 3 ), dtype of uint8
        output_file_path = string, path to image file
    OUTPUT:
        status = bool, True if write successfully, False otherwise
    '''
    if ( use_cv2 ) :
        status = cv2.imwrite( output_file_path, image_array )
    elif ( use_PIL ):
        pil_image = PIL.Image.fromarray( image_array )
        pil_image.save( output_file_path )
        status = os.path.isfile( output_file_path )
    return status

class ISIPPT( object ) :
    '''ISI-PPT-DATASET API
    '''
    def __init__( self, all_json_file_list, data_root = '', border_perc = 0.16 ) :
        assert os.path.isfile( all_json_file_list ), "ERROR: cannot locate input json file list %s" % all_json_file_list
        self.all_json_list = self._load_all_json_file( all_json_file_list, data_root )
        print "INFO: successfully load ISI-PPT dataset with", self.nb_samples, "samples"
        self.border_perc = border_perc
        return
    def _load_all_json_file( self, all_json_file_list, data_root ) :
        with open( all_json_file_list, 'r' ) as IN :
            json_files = [ os.path.join( data_root, line.strip() ) for line in IN.readlines() ]
        return json_files
    @property
    def nb_samples( self ) :
        return len( self.all_json_list )
    def _load_one_image( self, json_file ) :
        input_image_file = json_file.replace('json','jpg')
        image = imread( input_image_file )
        return image
    def _generate_one_target( self, json_file, border_perc = None ) :
        if ( border_perc is None ) :
            border_perc = self.border_perc
        slide_lut = json.load( open( json_file ) )
        image_height, image_width = [ slide_lut[key] for key in [ 'image_height', 'image_width'] ]
        nontext, text, border = [ np.zeros( ( image_height, image_width ), dtype = np.float32 ) for k in range( 3 )]
        nontext = 1 - nontext
        for bbox, trans in zip( slide_lut[ 'bounding_box'], slide_lut[ 'transcription' ] ) :
            x0, y0, w, h = bbox
            x1, y1 = x0+w+1, y0+h+1
            b = max( 1, int( h * border_perc + .5 ) )
            nontext[ y0:y1, x0:x1] = 0
            border[ y0:y1, x0:x1 ] = 1
            y0p, y1p = y0 + b, y1 - b
            x0p, x1p = x0 + b, x1 - b
            text[ y0p:y1p, x0p:x1p ] = 1
            border[ y0p:y1p, x0p:x1p ] = 0
        return np.dstack( [ nontext, border, text ] )
    def get_one_json( self, sample_idx = None ) :
        if ( sample_idx is not None ) and ( ( sample_idx >= self.nb_samples ) or ( sample_idx < 0 ) ) :
            print "WARNING: given sample index out of the valid range (0, %d), use a random sample instead" % self.nb_samples
            sample_idx = None
        if ( sample_idx is None ):
            sample_idx = np.random.randint( 0, self.nb_samples )
        this_json_file = self.all_json_list[ sample_idx ]
        return this_json_file, sample_idx
    def get_one_sample( self, sample_idx = None ) :
        this_json_file, sample_idx = self.get_one_json( sample_idx )
        image = self._load_one_image( this_json_file )
        target = self._generate_one_target( this_json_file )
        return image, target
    def save_target_to_disk( self, sample_idx, output_dir ) :
        image, target = self.get_one_sample( sample_idx )
        target_uint8 = target.astype('uint8') * 255
        target_bname = os.path.basename( self.all_json_list[ sample_idx ] ).replace( 'json', 'png' )
        target_file = os.path.join( output_dir, target_bname )
        return imwrite( target_uint8, target_file ), target_file
    def get_line_OCR_samples( self, sample_idx = None ) :
        this_json_file, sample_idx = self.get_one_json( sample_idx )
        input_image_file = this_json_file.replace('json','jpg')
        image = imread( input_image_file )
        slide_lut = json.load( open( this_json_file ) )
        samples, transcriptions = [], []
        for bbox, trans in zip( slide_lut[ 'bounding_box'], slide_lut[ 'transcription' ] ) :
            x0, y0, w, h = bbox
            x1, y1 = x0+w+1, y0+h+1
            samples.append( image[ y0:y1, x0:x1 ] )
            word_trans = filter( None, "".join( trans ).replace('_','').replace(' ','').split('u') )
            unicode_trans = self._remove_formatting_chars("".join( [ unichr( int( v, 16 ) ) for v in word_trans ] ) )
            transcriptions.append( unicode_trans )
        return samples, transcriptions
    def _remove_formatting_chars( self, trans ) :
        return "".join(ch for ch in trans if unicodedata.category(ch)[0]!="C")
    def get_line_height_distribution( self, sample_idx = None, nb_bins = 16, bin_width = 8 ) :
        this_json_file, sample_idx = self.get_one_json( sample_idx )
        slide_lut = json.load( open( this_json_file ) )
        hist = np.zeros([ nb_bins,], dtype = np.float32 )
        coef = 540./ slide_lut['image_height']
        for bbox in  slide_lut[ 'bounding_box'] :
            x0, y0, w, h = bbox
            if ( h ==  0 ) :
                continue
            bin_no = min( int( 2 * np.log( h * coef ) / np.log( 2 ) ), nb_bins - 1 )
            hist[ bin_no ] = float( w )
        return hist / np.sum( hist )
    def get_trans_language( self, sample_idx = None ) :
        this_json_file, sample_idx = self.get_one_json( sample_idx )
        slide_lut = json.load( open( this_json_file ) )
        language_lut = dict()
        for trans in slide_lut[ 'transcription' ] :
            word_trans = filter( None, "".join( trans ).replace('_','').replace(' ','').split('u') )
            unicode_trans = self._remove_formatting_chars( "".join( [ unichr( int( v, 16 ) ) for v in word_trans ] ) )
            for c in unicode_trans :
                try :
                    lang = unicodedata.name( c ).split(' ')[0]
                except Exception, e :
                    continue
                if ( not language_lut.has_key( lang ) ) :
                    language_lut[ lang ] = 0
                language_lut[ lang ] += 1
        keys = language_lut.keys()
        vals = [ language_lut[k] for k in keys ]
        if ( len( vals ) > 0 ) :
            dominant_idx = np.argmax( vals )
            return keys[ dominant_idx ]
        else :
            return 'UNK'
    def visualize_line_samples( self, line_images, line_trans ) :
        idx = 1
        for image, trans in zip( line_images, line_trans ) :
            coef = image.shape[1]/600.
            fsize = min( 30, max( 2, int( 30 * coef ) ) )
            pyplot.figure( figsize=( fsize, 6 * fsize ) ) 
            pyplot.imshow( image[:,:,::-1] )
            pyplot.title( "Line %d: %s" % ( idx, trans ), fontSize = 40 )
            idx += 1
        pyplot.show()
        return
    def visualize_one_raw_json( self, input_json_file ) :
        input_image_file = input_json_file.replace('json','jpg')
        image = imread( input_image_file )
        slide_lut = json.load( open( input_json_file ) )
        for bbox, trans in zip( slide_lut[ 'bounding_box'], slide_lut[ 'transcription' ] ) :
            x0, y0, w, h = bbox
            cv2.rectangle( image, ( x0, y0 ), ( x0 + w, y0 + h ), color = (0, 255, 0 ), thickness = 3 )
        pyplot.figure( figsize = (10,10) )
        pyplot.imshow( image[:,:,::-1] )
        pyplot.axis('off')
        pyplot.show()
        return
    def visualize_image_and_target( self, image, target ) :
        pyplot.figure( figsize = (15,15 ))
        pyplot.subplot(131)
        pyplot.imshow( image[:,:,::-1] )
        pyplot.title('original image: X')
        pyplot.axis('off')
        pyplot.subplot(132)
        if ( target.dtype == np.float32 ) :
            target = target.astype('uint8')
        pyplot.imshow( target * 255 )
        pyplot.title('3-class target: Y')
        pyplot.axis('off')
        pyplot.subplot(133)
        pyplot.imshow( image[:,:,::-1] * target )
        pyplot.title('X * Y')
        pyplot.axis('off')
        pyplot.show()
        return
    def visualize_line_height_distribution( self, lh_hist_bins ) :
        nb_bins = len( lh_hist_bins )
        pyplot.figure( figsize = (10,5) )
        pyplot.bar( range( nb_bins ), lh_hist_bins )
        pyplot.xlabel( '2 * log_2( LH )')
        pyplot.ylabel( 'data percentage' )
        pyplot.title( 'sample line height distribution')
        pyplot.xlim( [0, nb_bins] )
        pyplot.show()
        return

