# ISI-PPT-Dataset

## Dataset Overview
This is a Dataset for Arabic/English text detection and optical character recognition. All image data are text-slides extracted from PowerPoint files downloaded from Internet through the Google API. The used Arabic keywords are translated from the following English keywords through Google Translator: *science, technology, wedding, university, biology, computer science, consumer electronics, kung fu, olympics, arts, museum, architecture, food, disease, travel, finance, car, foreign affairs, education, family, literature, books, united states, sailing, airplane, cellphone, skin, religion, fashion, cosmetics, mechanical engineering, mathematics, english, united nations, sun, women, football, basketball, army, parade, new year, christmas, retirement, company, business, case study, plan, deep learning, arabic*.

All annotations are automatically generated mainly through the WinCom32 Python API. Postprocess is also applied to place a more accurate text bounding boxe or to suppress *false-alarms*, e.g. a text box only containing spaces. Finally, all annotation results are briefly reviewed by human to reject extreme bad samples, e.g. a slide with a large portion of copied table as image. In summary, this dataset contains 10,692 images, and roughly 100K line samples. 

## Dataset Structure
The overview of directory hiearchy is shown below:
```
    |---- data: [ containing all ISI-PPT (image, annotation) pairs ]
            |---- $md5code.jpg  # image file
            |---- $md5code.json # annotation file
    |---- notebooks : [ containing a simple API and a Py2 notebook ]
            |---- isi_ppt_api.py # simple API to load/dump/visualize dataset
            |---- ISI-PPT-Demonstration.ipynb # Py2 notebook showing basic usage
    |---- docs : [ containing cached images for this README ]
    |---- isi_ppt_json.list : data list of ISI-PPT dataset
    |---- README.md : this file
```

## Data Details
for each sample in the ISI-PPT Dataset, it is a tuple of `( image_file, label_file )`, where:
1. `image_file` is named by using the md5 code of the image array, and always ends with the `jpg` suffix.
2. `label_file` is named by using the same image md5 code, and always ends with the `json` suffix.
3. each `label_file` contains the following fields:

- `bounding_box` : text bounding box list, each element is a text bounding box of format `( box_left, box_top, box_width, box_height )`.
- `transcription` : text transcription list, each element is a unicode string transcribing the content of a corresponding text bounding box
- `image_height` : height of `image_file`
- `image_width` : width of `image_file`

## Example
### A more comprehensive demo can be found in [`notebooks/ISI-PPT-Demonstration.ipynb`](https://gitlab.com/rex-yue-wu/ISI-PPT-Dataset/blob/master/notebooks/ISI-PPT-Demonstration.ipynb). 
The snippet below shows the basic usage of loading the ISI-PPT dataset, and generating 8 random samples with the three-class target annotation, namely
- ![#ff0000](https://placehold.it/15/ff0000/000000?text=+) `non-text`
- ![#00ff00](https://placehold.it/15/00ff00/000000?text=+) `border`
- ![#0000ff](https://placehold.it/15/0000ff/000000?text=+) `text`

You may modify the parameter `border_perc` to adjust the expected border width perctange (w.r.t. line height).

```python
# load required libs
import os
import isi_ppt_api
from matplotlib import pyplot

# create an ISI-PPT API instance
isi_ppt_data_root = '../data' # file path to the root of ISI-PPT dataset
border_perc = 0.16 # percentage of text border used for target generation
isi_ppt_json_file_list = os.path.join( isi_ppt_data_root, os.path.pardir, 'isi_ppt_json.list' )
assert os.path.isfile( isi_ppt_json_file_list ), "ERROR: fail to locate input dataset file, check $isi_ppt_data_root"
engine = isi_ppt_api.ISIPPT( all_json_file_list = isi_ppt_json_file_list, 
                             data_root = isi_ppt_data_root, 
                             border_perc = border_perc )
# visualize 8 random samples
nb_random_samples = 8
pyplot.figure( figsize=(20,40) )
idx = 0
for k in range(nb_random_samples) :
    random_json_file, sample_idx = engine.get_one_json()
    image, target = engine.get_one_sample( sample_idx = sample_idx )
    image = image[:,:,::-1]
    img_list = [ image, target, target.astype('uint8') * image ]
    title_list = [ 'image', 'target', 'image * target']
    for name, img in zip( title_list, img_list ) :
        idx += 1
        pyplot.subplot( nb_random_samples, 3, idx )
        pyplot.imshow( img )
        pyplot.axis('off')
        pyplot.title( 'sample %d %s' % ( k, name ) )
```
![sample results](https://gitlab.com/rex-yue-wu/ISI-PPT-Dataset/raw/master/docs/sample.png)

## Citation
This dataset is used to train the baseline text-detection system for `Self-organized Text Detection with Minimal Post-processing via Border Learning`. 
If you use this dataset in any publication, please kindly cite our paper below.

    @inproceedings{wu2017iccv,
        author = {Yue Wu and Prem Natarajan},
        booktitle = {International Conference on Computer Vision},
        title = {Self-organized Text Detection with Minimal Post-processing via Border Learning},
        year = {2017}
    }
A pretrained text detection model using this dataset can be found in [ISI-PPT-Text-Detector: git@gitlab.com:rex-yue-wu/ISI-PPT-Text-Detector.git](https://gitlab.com/rex-yue-wu/ISI-PPT-Text-Detector).

## Information
- Contact: Dr. Yue Wu 
- Email: `yue_wu@isi.edu`
- Affiliation: [The USC Information Sciences Institute](https://www.isi.edu/), Marina Del Rey, CA 90292, USA